import React,{useState,useEffect} from 'react';
import axios from 'axios';
import Slider from "react-slick";
import '../scss/App.scss';
import '../scss/font.scss';
import '../scss/blog.scss';
import '../scss/about.scss';
import {Card,Button,Container,Form,Col,InputGroup,FormControl,Modal,Breadcrumb} from 'react-bootstrap'
// import { MDBCol, MDBFormInline, MDBIcon } from "mdbreact";
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import blog1 from "../images/blog/blog1.jpeg";
import search from "../images/search.png";
import fb from "../images/facebook.png";
import wp from "../images/whatsapp.png";
import yb from "../images/youtube.png";
import insta from "../images/instagram.png";
import calendar from "../images/schedule.png";
import comment from "../images/comments.png";

function Blogs() {
  const url="http://localhost:8080/api/create/consultation"
   const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
   const [posts,setPosts]=useState([])
   const [consultent,setConsultent]=useState({
     BirthHour:"",
     BirthPlace:"",
     Date_of_birth:"",
     Gender:"",
     Mobile_no:"",
     Email:"",
     Name:""
   })
    // const [id,setId]=useState(1)

    useEffect(()=>{
        axios.get('http://localhost:8080/api/getAllBlogs')
        .then(res=>{
            console.log(res);
            setPosts(res.data)
            console.log((res.data))
        })
        .catch(err=>{
            console.log(err);
        })
    },[])

   function submit(e){
    e.preventDefault();
    console.log(e)
    axios.post(url,{
       BirthHour:consultent.BirthHour,
     BirthPlace:consultent.BirthPlace,
     Date_of_birth:consultent.Date_of_birth,
     Gender:consultent.Gender,
     Mobile_no:consultent.Mobile_no,
     Email:consultent.Email,
     Name:consultent.Name
    }
   
      )
      .then(res=>{
        console.log(res.data)
        alert("Successfully inserted");
      })
      alert("Successfully inserted");
     handleClose();  
   }

    function handle(e){
      const newData={...consultent}
      newData[e.target.id]=e.target.value
      setConsultent(newData)
      console.log(newData)
    }
    const getDate = (date) => {
    var options = { year: 'numeric', month: 'long', day: 'numeric' };
    return new Date(date).toLocaleDateString([],options);
    }
  return (
    
      <>
     
      <Navbar />
      <div>
     
          <div className="header_image">
          
           {/* <span >Blog</span> */}
           <Breadcrumb className="header_text">
  <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
  <Breadcrumb.Item href="/blog">
   Our Blogs
  </Breadcrumb.Item>
  <Breadcrumb.Item active>Mundane Astrology </Breadcrumb.Item>
</Breadcrumb>
        </div>
    <div className="blogContainer" id="blog">

        <div className="left">
             <InputGroup className="mb-2 mr-sm-2">
              <InputGroup.Prepend>
                <InputGroup.Text><img src={search} alt="search" style={{width:15,height:15}} /></InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl id="inlineFormInputGroupUsername2" placeholder="Search" />
            </InputGroup>
            <div className="category">
              <h5>CATEGORIES</h5>
              <p className="categoris">Gemstone (504)</p>
              <p className="categoris">Medical Astrology (79)</p>
              <p className="categoris">Astrology (345)</p>
              <p className="categoris">Rudraksha (69)</p>
              <p className="categoris">Horoscope (61)</p>
            </div>
          
          
            {/* <h4 className="blogHeading">GET FREE CONSULTATION</h4> */}
             <Button variant="primary" onClick={handleShow} style={{marginTop:'20%'}}>
       GET FREE CONSULTATION
      </Button>
       <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Get Free Consultent</Modal.Title>
        </Modal.Header>
        <Modal.Body>
                 <Form onSubmit={(e)=>submit(e)}>
                 <Form.Group>
    <Form.Control type="text" placeholder="Name" id="Name" required value={consultent.Name} onChange={(e)=> handle(e)} />
  </Form.Group>
  <Form.Group>
    <Form.Control type="email" required placeholder="Email" id="Email" value={consultent.Email} onChange={(e)=> handle(e)} />
  </Form.Group>
    <Form.Group>
    <Form.Control type="text" required placeholder="Mobile No." id="Mobile_no" value={consultent.Mobile_no} onChange={(e)=> handle(e)}/>
  </Form.Group>
    <Form.Group>
    <Form.Control type="text" required placeholder="Gender" id="Gender" value={consultent.Gender} onChange={(e)=> handle(e)}/>
  </Form.Group>
    <Form.Group>
    <Form.Control type="text" required placeholder="Date of Birth" id="Date_of_birth" value={consultent.Date_of_birth} onChange={(e)=> handle(e)}/>
  </Form.Group>
    <Form.Group>
    <Form.Control type="text" required placeholder="Birth Place" id="BirthPlace" value={consultent.BirthPlace} onChange={(e)=> handle(e)}/>
  </Form.Group>
    <Form.Group>
    <Form.Control type="text" required placeholder="Birth Hour" id="BirthHour" value={consultent.BirthHour} onChange={(e)=> handle(e)}/>
  </Form.Group>
</Form>

        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onSubmit={(e)=>submit(e)} value="Submit">
            Submit
          </Button>
        </Modal.Footer>
      </Modal>

       
        </div>
        <div className="right">
           {posts.map(post => { 
            //  console.log(post._id['4'])
            return(
           <div>
                  <img src={`http://localhost:8080/api/getphoto/${post._id}`} alt="blog1" className="blog1"/>
                  <h1 className="blogHeading">{post.Title}</h1>
                  <div className="social" style={{justifyContent:'flex-start',marginBottom:'2%',marginTop:'3%'}}>
            <div className="calendar">
              <img src={calendar} alt="calendar" />
          <Card.Text style={{color:'blue'}} className="blog_content">{getDate(post.createdAt)}</Card.Text>
          </div>
          <div className="comment" style={{marginLeft:'5%'}}>
              <img src={comment} alt="comment" />
          <Card.Text style={{color:'blue'}} className="blog_content">0</Card.Text>
          </div>
          </div>
            <p>{post.Content}</p>
           <div style={{display:'flex',flexDirection:'row',marginRight:'10px'}}>
            <p>SHARE THIS ON:</p>
              <div className="icon">
          <a href=""><img src={fb} alt="fb" /></a>
          <a href=""><img src={wp} alt="wp" /></a>
            <a href=""><img src={insta} alt="insta" /></a>
          <a href=""><img src={yb} alt="yb" /></a>
          </div>
          </div>
                  <p>POSTED IN: Mundane Astrology</p>
                  <h2 className="blogHeading">LEAVE A REPLY</h2>
                  <Form>
                        <Form.Group controlId="formBasicEmail">
          <Form.Control type="textarea" placeholder="Comment" />
        </Form.Group>
                      <Form.Group controlId="formBasicEmail">
                            <Form.Row>
            <Col>
              <Form.Control type="text" placeholder="Name" />
            </Col>
            <Col>
              <Form.Control type="email" placeholder="Email" />
            </Col>
          </Form.Row>
          
          </Form.Group>
          <Form.Group controlId="formBasicEmail">
            <Form.Control type="text" placeholder="Website" />
          </Form.Group>
            <Form.Group controlId="formBasicEmail">
            <Form.Label className="blogHeading">18 − sixteen =</Form.Label>
            <Form.Control type="text" />
          </Form.Group>
          <Button variant="primary" type="submit">
            Post Comment
          </Button>
        </Form>
        </div>
         );
         })}
        </div>
        <div className="middle">
           <div className="category" style={{marginTop:'0%'}}>
              <h5>RECENT POSTS</h5>
              <p className="categoris">Gemstone (504)</p>
              <p className="categoris">Medical Astrology (79)</p>
              <p className="categoris">Astrology (345)</p>
              <p className="categoris">Rudraksha (69)</p>
              <p className="categoris">Horoscope (61)</p>
            </div>
           <div className="category">
              <h5>RECENT COMMENTS</h5>
              <p className="categoris">Gemstone (504)</p>
              <p className="categoris">Medical Astrology (79)</p>
              <p className="categoris">Astrology (345)</p>
              <p className="categoris">Rudraksha (69)</p>
              <p className="categoris">Horoscope (61)</p>
            </div>
          
        </div>
         
    </div>
   {/* ); })} */}
   </div>
    <Footer />
    </>
 );
}

export default Blogs;
