import React,{useState,useEffect} from 'react';
import axios from 'axios';
import {Card,Button,Container} from 'react-bootstrap'
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import '../scss/App.scss';
import '../scss/font.scss';
import '../scss/about.scss';
import '../scss/blog.scss';
import blog1 from "../images/blog/blog1.jpeg";
import blog2 from "../images/blog/blog2.jpg";
import blog3 from "../images/blog/blog3.jpg";
import blog4 from "../images/blog/blog4.jpg";
import blog5 from "../images/blog/blog5.png";
import blog6 from "../images/blog/blog6.jpg";
import blog7 from "../images/blog/blog7.jpg";
import blog8 from "../images/blog/blog8.jpg";
import blog9 from "../images/blog/blog9.jpg";
import blog10 from "../images/blog/blog10.jpg";
import blog11 from "../images/blog/blog11.jpeg";
import blog12 from "../images/blog/blog12.jpg";
import fb from "../images/facebook.png";
import wp from "../images/whatsapp.png";
import yb from "../images/youtube.png";
import insta from "../images/instagram.png";
import calendar from "../images/calendar.png";
import comment from "../images/comment.png";
import blog from '../components/Blog';
function BlogScreen() {
   const [posts,setPosts]=useState([])
   useEffect(()=>{
        axios.get('http://localhost:8080/api/getAllBlogs')
        .then(res=>{
            console.log(res);
            setPosts(res.data)
            console.log((res.data))
        })
        .catch(err=>{
            console.log(err);
        })
    },[])
     const getDate = (date) => {
    var options = { year: 'numeric', month: 'long', day: 'numeric' };
    return new Date(date).toLocaleDateString([],options);
    }
  return (
    <div className="BlogScreen">
    <Navbar />
     <div className="header_image">
           <span className="header_text">Blogs</span>
        </div>
         
    <div className="Blogrow animate__animated animate__bounce" style={{display:'flex',flexDirection:'row',justifyContent:'space-evenly',marginBottom:'2%',marginTop:'5%',flexWrap:'wrap'}}>
      {posts.map(post => {
         return(
            <div> 
      <Card style={{ width: '22rem',height:'32rem' }}>
  <Card.Img variant="top" src={`http://localhost:8080/api/getphoto/${post._id}`} style={{height:200}} />
  <Card.Body>
    <Card.Title className="blog_title">{post.Title}</Card.Title>
    <div className="social">
      <div className="calendar">
        <img src={calendar} alt="calendar" />
    <Card.Text style={{color:'blue'}} className="blog_content">{getDate(post.createdAt)}</Card.Text>
    </div>
     <div className="comment">
        <img src={comment} alt="comment" />
    <Card.Text style={{color:'blue'}} className="blog_content">0</Card.Text>
    </div>
    </div>
    <Card.Text style={{color:'black'}} className="blog_content">
{post.Content}
    </Card.Text>
 <div className="social">
    <Button variant="primary" className="blog_read_more"><a href="/blogs" style={{color:'white',fontSize:'.8rem'}}>Read More</a></Button>
    <div className="icon">
    <a href=""><img src={fb} alt="fb" /></a>
     <a href=""><img src={wp} alt="wp" /></a>
      <a href=""><img src={insta} alt="insta" /></a>
     <a href=""><img src={yb} alt="yb" /></a>
    </div>
    </div>  </Card.Body>
</Card>
</div>
 );  })}
<Card style={{ width: '22rem',height:'32rem' }}>
  <Card.Img variant="top" src={blog2} style={{height:200}} />
  <Card.Body>
    <Card.Title className="blog_title">Sharad Pawar Horoscope</Card.Title>
    <div className="social">
      <div className="calendar">
        <img src={calendar} alt="calendar" />
    <Card.Text style={{color:'blue'}} className="blog_content">Jan 9, 2019</Card.Text>
    </div>
     <div className="comment">
        <img src={comment} alt="comment" />
    <Card.Text style={{color:'blue'}} className="blog_content">0</Card.Text>
    </div>
    </div>
    <Card.Text style={{color:'black'}} className="blog_content">
    Astrological analysis of Sharad Pawar’s horoscope can give answers to few questions. First would be his political career i.e. When will it end? The second  would be about his successor. Whether his nephew will continue further or this nepotism will end. The third about his health. Let’s begin with his political career The horoscope of […]
    </Card.Text>
    <div className="social">
    <Button variant="primary" className="blog_read_more"><a href="/blogs" style={{color:'white',fontSize:'.8rem'}}>Read More</a></Button>
    <div className="icon">
    <a href=""><img src={fb} alt="fb" /></a>
     <a href=""><img src={wp} alt="wp" /></a>
      <a href=""><img src={insta} alt="insta" /></a>
     <a href=""><img src={yb} alt="yb" /></a>
    </div>
    </div>
  </Card.Body>
</Card>
<Card style={{ width: '22rem',height:'32rem' }}>
  <Card.Img variant="top" src={blog3} style={{height:200}} />
  <Card.Body>
    <Card.Title className="blog_title">Earthquake Astrology 4 April 2021</Card.Title>
    <div className="social">
      <div className="calendar">
        <img src={calendar} alt="calendar" />
    <Card.Text style={{color:'blue'}} className="blog_content">Jan 9, 2019</Card.Text>
    </div>
     <div className="comment">
        <img src={comment} alt="comment" />
    <Card.Text style={{color:'blue'}} className="blog_content">0</Card.Text>
    </div>
    </div>
    <Card.Text style={{color:'black'}} className="blog_content">
     Vande Mataram Friends! Today again I come up with the earthquake prediction. I have predicted these predictions previously with 98% date and time accuracy I don’t want to afraid anyone with these predictions but just want to give a soft warning so you can take all necessary precautions which is the very first reason Secondly, […]
    </Card.Text>
 <div className="social">
    <Button variant="primary" className="blog_read_more"><a href="/blogs" style={{color:'white',fontSize:'.8rem'}}>Read More</a></Button>
    <div className="icon">
    <a href=""><img src={fb} alt="fb" /></a>
     <a href=""><img src={wp} alt="wp" /></a>
      <a href=""><img src={insta} alt="insta" /></a>
     <a href=""><img src={yb} alt="yb" /></a>
    </div>
    </div>  </Card.Body>
</Card>

</div>


     <div className="Blogrow animate__animated animate__bounce" style={{display:'flex',flexDirection:'row',justifyContent:'space-evenly',marginBottom:'2%',flexWrap:'wrap'}}>
      <Card style={{ width: '22rem',height:'32rem' }}>
  <Card.Img variant="top" src={blog4} style={{height:200}} />
  <Card.Body>
    <Card.Title className="blog_title">Finance Astrology Report For Aries</Card.Title>
   <div className="social">
      <div className="calendar">
        <img src={calendar} alt="calendar" />
    <Card.Text style={{color:'blue'}} className="blog_content">Jan 9, 2019</Card.Text>
    </div>
     <div className="comment">
        <img src={comment} alt="comment" />
    <Card.Text style={{color:'blue'}} className="blog_content">0</Card.Text>
    </div>
    </div>
    <Card.Text style={{color:'black'}} className="blog_content">
    Finance astrology report for Aries tells a lot about the money matter. The Initial months of the year promises huge financial gains. There are chances of sudden luck and profit. One can carry on with the objective of purchasing or selling a property if it’s on the mind. You can try it before March. Skillful […]
    </Card.Text>
 <div className="social">
    <Button variant="primary" className="blog_read_more"><a href="/blogs" style={{color:'white',fontSize:'.8rem'}}>Read More</a></Button>
    <div className="icon">
    <a href=""><img src={fb} alt="fb" /></a>
     <a href=""><img src={wp} alt="wp" /></a>
      <a href=""><img src={insta} alt="insta" /></a>
     <a href=""><img src={yb} alt="yb" /></a>
    </div>
    </div>  </Card.Body>
</Card>
<Card style={{ width: '22rem',height:'32rem' }}>
  <Card.Img variant="top" src={blog5} style={{height:200}} />
  <Card.Body>
    <Card.Title className="blog_title">Sharad Pawar Horoscope</Card.Title>
    <div className="social">
      <div className="calendar">
        <img src={calendar} alt="calendar" />
    <Card.Text style={{color:'blue'}} className="blog_content">Jan 9, 2019</Card.Text>
    </div>
     <div className="comment">
        <img src={comment} alt="comment" />
    <Card.Text style={{color:'blue'}} className="blog_content">0</Card.Text>
    </div>
    </div>
    <Card.Text style={{color:'black'}} className="blog_content">
Moonstone is a gem that has derived its properties from planet Moon. It is a semi-precious stone or upratna and it has been alluring the man from prehistoric times. It is extracted from feldspar which is a common mineral and it possesses remarkable properties to benefits its wearer in several ways. Moonstone is a divine […]    </Card.Text>
 <div className="social">
    <Button variant="primary" className="blog_read_more"><a href="/blogs" style={{color:'white',fontSize:'.8rem'}}>Read More</a></Button>
    <div className="icon">
    <a href=""><img src={fb} alt="fb" /></a>
     <a href=""><img src={wp} alt="wp" /></a>
      <a href=""><img src={insta} alt="insta" /></a>
     <a href=""><img src={yb} alt="yb" /></a>
    </div>
    </div>  </Card.Body>
</Card>
<Card style={{ width: '22rem',height:'32rem' }}>
  <Card.Img variant="top" src={blog6} style={{height:200}} />
  <Card.Body>
    <Card.Title className="blog_title">Earthquake Astrology 4 April 2021</Card.Title>
   <div className="social">
      <div className="calendar">
        <img src={calendar} alt="calendar" />
    <Card.Text style={{color:'blue'}} className="blog_content">Jan 9, 2019</Card.Text>
    </div>
     <div className="comment">
        <img src={comment} alt="comment" />
    <Card.Text style={{color:'blue'}} className="blog_content">0</Card.Text>
    </div>
    </div>
    <Card.Text style={{color:'black'}} className="blog_content">
     Vande Mataram Friends! Today again I come up with the earthquake prediction. I have predicted these predictions previously with 98% date and time accuracy I don’t want to afraid anyone with these predictions but just want to give a soft warning so you can take all necessary precautions which is the very first reason Secondly, […]
    </Card.Text>
 <div className="social">
    <Button variant="primary" className="blog_read_more"><a href="/blogs" style={{color:'white',fontSize:'.8rem'}}>Read More</a></Button>
    <div className="icon">
    <a href=""><img src={fb} alt="fb" /></a>
     <a href=""><img src={wp} alt="wp" /></a>
      <a href=""><img src={insta} alt="insta" /></a>
     <a href=""><img src={yb} alt="yb" /></a>
    </div>
    </div>  </Card.Body>
</Card>

</div>
     <div className="Blogrow animate__animated animate__bounce" style={{display:'flex',flexDirection:'row',justifyContent:'space-evenly',marginBottom:'2%',flexWrap:'wrap'}}>
      <Card style={{ width: '22rem',height:'32rem' }}>
  <Card.Img variant="top" src={blog7} style={{height:200}} />
  <Card.Body>
    <Card.Title className="blog_title">Saturn Retrograde 2021</Card.Title>
    <div className="social">
      <div className="calendar">
        <img src={calendar} alt="calendar" />
    <Card.Text style={{color:'blue'}} className="blog_content">Jan 9, 2019</Card.Text>
    </div>
     <div className="comment">
        <img src={comment} alt="comment" />
    <Card.Text style={{color:'blue'}} className="blog_content">0</Card.Text>
    </div>
    </div>
    <Card.Text style={{color:'black'}} className="blog_content">
     On 23rd May 2021, the planet
      Saturn will be retrograded in
       Capricorn itself. It is seen 
       that whenever planet Jupiter
        and Saturn transits, it brings
     some massive changes on earth as well as on mankind. 
     Saturn retrogression in Capricorn
      always brings lots of changes
       like changes of generation
        It is evident, as we are witnessing […]
    </Card.Text>
 <div className="social">
    <Button variant="primary" className="blog_read_more"><a href="/blogs" style={{color:'white',fontSize:'.8rem'}}>Read More</a></Button>
    <div className="icon">
    <a href=""><img src={fb} alt="fb" /></a>
     <a href=""><img src={wp} alt="wp" /></a>
      <a href=""><img src={insta} alt="insta" /></a>
     <a href=""><img src={yb} alt="yb" /></a>
    </div>
    </div>  </Card.Body>
</Card>
<Card style={{ width: '22rem',height:'32rem' }}>
  <Card.Img variant="top" src={blog8} style={{height:200}} />
  <Card.Body>
    <Card.Title className="blog_title">Sharad Pawar Horoscope</Card.Title>
    <div className="social">
      <div className="calendar">
        <img src={calendar} alt="calendar" />
    <Card.Text style={{color:'blue'}} className="blog_content">Jan 9, 2019</Card.Text>
    </div>
     <div className="comment">
        <img src={comment} alt="comment" />
    <Card.Text style={{color:'blue'}} className="blog_content">0</Card.Text>
    </div>
    </div>
    <Card.Text style={{color:'black'}} className="blog_content">
    Astrological analysis of Sharad Pawar’s horoscope can give answers to few questions. First would be his political career i.e. When will it end? The second  would be about his successor. Whether his nephew will continue further or this nepotism will end. The third about his health. Let’s begin with his political career The horoscope of […]
    </Card.Text>
 <div className="social">
    <Button variant="primary" className="blog_read_more"><a href="/blogs" style={{color:'white',fontSize:'.8rem'}}>Read More</a></Button>
    <div className="icon">
    <a href=""><img src={fb} alt="fb" /></a>
     <a href=""><img src={wp} alt="wp" /></a>
      <a href=""><img src={insta} alt="insta" /></a>
     <a href=""><img src={yb} alt="yb" /></a>
    </div>
    </div>  </Card.Body>
</Card>
<Card style={{ width: '22rem',height:'32rem' }}>
  <Card.Img variant="top" src={blog9} style={{height:200}} />
  <Card.Body>
    <Card.Title className="blog_title">Earthquake Astrology 4 April 2021</Card.Title>
   <div className="social">
      <div className="calendar">
        <img src={calendar} alt="calendar" />
    <Card.Text style={{color:'blue'}} className="blog_content">Jan 9, 2019</Card.Text>
    </div>
     <div className="comment">
        <img src={comment} alt="comment" />
    <Card.Text style={{color:'blue'}} className="blog_content">0</Card.Text>
    </div>
    </div>
    <Card.Text style={{color:'black'}} className="blog_content">
     Vande Mataram Friends! Today again I come up with the earthquake prediction. I have predicted these predictions previously with 98% date and time accuracy I don’t want to afraid anyone with these predictions but just want to give a soft warning so you can take all necessary precautions which is the very first reason Secondly, […]
    </Card.Text>
 <div className="social">
    <Button variant="primary" className="blog_read_more"><a href="/blogs" style={{color:'white',fontSize:'.8rem'}}>Read More</a></Button>
    <div className="icon">
    <a href=""><img src={fb} alt="fb" /></a>
     <a href=""><img src={wp} alt="wp" /></a>
      <a href=""><img src={insta} alt="insta" /></a>
     <a href=""><img src={yb} alt="yb" /></a>
    </div>
    </div>  </Card.Body>
</Card>

</div>
     <div className="Blogrow animate__animated animate__bounce" style={{display:'flex',flexDirection:'row',justifyContent:'space-evenly',flexWrap:'wrap'}}>
      <Card style={{ width: '22rem',height:'32rem' }}>
  <Card.Img variant="top" src={blog10} style={{height:200}} />
  <Card.Body>
    <Card.Title className="blog_title">Saturn Retrograde 2021</Card.Title>
    <div className="social">
      <div className="calendar">
        <img src={calendar} alt="calendar" />
    <Card.Text style={{color:'blue'}} className="blog_content">Jan 9, 2019</Card.Text>
    </div>
     <div className="comment">
        <img src={comment} alt="comment" />
    <Card.Text style={{color:'blue'}} className="blog_content">0</Card.Text>
    </div>
    </div>
    <Card.Text style={{color:'black'}} className="blog_content">
     On 23rd May 2021, the planet
      Saturn will be retrograded in
       Capricorn itself. It is seen 
       that whenever planet Jupiter
        and Saturn transits, it brings
     some massive changes on earth as well as on mankind. 
     Saturn retrogression in Capricorn
      always brings lots of changes
       like changes of generation
        It is evident, as we are witnessing […]
    </Card.Text>
 <div className="social">
    <Button variant="primary" className="blog_read_more"><a href="/blogs" style={{color:'white',fontSize:'.8rem'}}>Read More</a></Button>
    <div className="icon">
    <a href=""><img src={fb} alt="fb" /></a>
     <a href=""><img src={wp} alt="wp" /></a>
      <a href=""><img src={insta} alt="insta" /></a>
     <a href=""><img src={yb} alt="yb" /></a>
    </div>
    </div>  </Card.Body>
</Card>
<Card style={{ width: '22rem',height:'32rem' }}>
  <Card.Img variant="top" src={blog11} style={{height:200}} />
  <Card.Body>
    <Card.Title className="blog_title">Sharad Pawar Horoscope</Card.Title>
    <div className="social">
      <div className="calendar">
        <img src={calendar} alt="calendar" />
    <Card.Text style={{color:'blue'}} className="blog_content">Jan 9, 2019</Card.Text>
    </div>
     <div className="comment">
        <img src={comment} alt="comment" />
    <Card.Text style={{color:'blue'}} className="blog_content">0</Card.Text>
    </div>
    </div>
    <Card.Text style={{color:'black'}} className="blog_content">
    Astrological analysis of Sharad Pawar’s horoscope can give answers to few questions. First would be his political career i.e. When will it end? The second  would be about his successor. Whether his nephew will continue further or this nepotism will end. The third about his health. Let’s begin with his political career The horoscope of […]
    </Card.Text>
 <div className="social">
    <Button variant="primary" className="blog_read_more"><a href="/blogs" style={{color:'white',fontSize:'.8rem'}}>Read More</a></Button>
    <div className="icon">
    <a href=""><img src={fb} alt="fb" /></a>
     <a href=""><img src={wp} alt="wp" /></a>
      <a href=""><img src={insta} alt="insta" /></a>
     <a href=""><img src={yb} alt="yb" /></a>
    </div>
    </div>  </Card.Body>
</Card>
<Card style={{ width: '22rem',height:'32rem' }}>
  <Card.Img variant="top" src={blog12} style={{height:200}} />
  <Card.Body>
    <Card.Title className="blog_title">Earthquake Astrology 4 April 2021</Card.Title>
   <div className="social">
      <div className="calendar">
        <img src={calendar} alt="calendar" />
    <Card.Text style={{color:'blue'}} className="blog_content">Jan 9, 2019</Card.Text>
    </div>
     <div className="comment">
        <img src={comment} alt="comment" />
    <Card.Text style={{color:'blue'}} className="blog_content">0</Card.Text>
    </div>
    </div>
    <Card.Text style={{color:'black'}} className="blog_content">
     Vande Mataram Friends! Today again I come up with the earthquake prediction. I have predicted these predictions previously with 98% date and time accuracy I don’t want to afraid anyone with these predictions but just want to give a soft warning so you can take all necessary precautions which is the very first reason Secondly, […]
    </Card.Text>
 <div className="social">
    <Button variant="primary" className="blog_read_more"><a href="/blogs" style={{color:'white',fontSize:'.8rem'}}>Read More</a></Button>
    <div className="icon">
    <a href=""><img src={fb} alt="fb" /></a>
     <a href=""><img src={wp} alt="wp" /></a>
      <a href=""><img src={insta} alt="insta" /></a>
     <a href=""><img src={yb} alt="yb" /></a>
    </div>
    </div>  </Card.Body>
</Card>

</div>
<Footer />
    
    </div>
  );
}

export default BlogScreen;
