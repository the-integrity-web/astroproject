import React from "react";
import {Breadcrumb} from 'react-bootstrap'
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import '../scss/services.scss';
import landr1 from '../images/services/landr1.png';
import landr2 from '../images/services/landr2.jpg';
import landr3 from '../images/services/landr3.jpg';
import landr4 from '../images/services/landr4.jpg';
import landr5 from '../images/services/landr5.jpg';
import landr6 from '../images/services/landr6.jpg';
import landr7 from '../images/services/landr7.jpg';
function OurServices() {
  return (
  <>
    <Navbar />
     <div className="header">
             <Breadcrumb className="bread">
            <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
            <Breadcrumb.Item active>Love and Relationship </Breadcrumb.Item>
            </Breadcrumb>
        </div>
    <div className="landrrow">
        <div className="landrcolumn">
            <h4>CATEGORIES</h4>
            <hr></hr>
            <p>Astrology Reports 2020</p>
            <p>Bollywood Astrology</p>
            <p>Love and Relationship</p>
            <p>Child Astrology</p>
            <p>Corporate Astrology</p>
            <p>Counselling</p>
        </div>
         <div className="landrcolumn1">
            <a href="/product"> <div class="card mb-3"  style={{maxWidth: '540px',textAlign:'center',alignContent:'center',padding:'3%',boxShadow:'inset 0 0 10px blue'}}>
            <div class="row g-0">
                <div class="col-md-4">
                <img src={landr4} alt="landr4" style={{width:150,height:150,borderRadius:'50%'}} />
                </div>
                <div class="col-md-8"  style={{alignSelf:'center'}}>
                <div class="card-body">
                    <h5 class="card-title" style={{color:'black'}}>How Will Be My Married Life</h5>
                    <p class="card-text" style={{color:'black'}}><span style={{textDecoration:'line-through'}}>₹5,400.00</span> ₹3,600.00</p>
                </div>
                </div>
            </div>
            </div></a>
            <a href="/product"> <div class="card mb-3" style={{maxWidth: '540px',textAlign:'center',alignContent:'center',padding:'3%',boxShadow:'inset 0 0 10px blue'}}>
            <div class="row g-0">
                <div class="col-md-4">
                <img src={landr5} alt="landr5" style={{width:150,height:150,borderRadius:'50%'}} />
                </div>
                <div class="col-md-8"  style={{alignSelf:'center'}}>
                <div class="card-body">
                    <h5 class="card-title" style={{color:'black'}}>Know Your Spouse From Your Horoscope</h5>
                    <p class="card-text" style={{color:'black'}}><span style={{textDecoration:'line-through'}}>₹5,400.00</span> ₹3,600.00</p>
                </div>
                </div>
            </div>
            </div></a>
           <a href="/product"> <div class="card mb-3" style={{maxWidth: '540px',textAlign:'center',alignContent:'center',padding:'3%',boxShadow:'inset 0 0 10px blue'}}>
            <div class="row g-0">
                <div class="col-md-4">
                <img src={landr6} alt="landr6" style={{width:150,height:150,borderRadius:'50%'}} />
                </div>
                <div class="col-md-8"  style={{alignSelf:'center'}}>
                <div class="card-body">
                    <h5 class="card-title" style={{color:'black'}}>Compatibility Analysis</h5>
                    <p class="card-text" style={{color:'black'}}><span style={{textDecoration:'line-through'}}>₹5,400.00</span> ₹3,600.00</p>
                </div>
                </div>
            </div>
            </div></a>
            <a href="/product"> <div class="card mb-3" style={{maxWidth: '540px',textAlign:'center',alignContent:'center',padding:'3%',boxShadow:'inset 0 0 10px blue'}}>
            <div class="row g-0">
                <div class="col-md-4">
                <img src={landr7} alt="landr7" style={{width:150,height:150,borderRadius:'50%'}} />
                </div>
                <div class="col-md-8"  style={{alignSelf:'center'}}>
                <div class="card-body">
                    <h5 class="card-title" style={{color:'black'}}>Muhurata For Marriage</h5>
                    <p class="card-text" style={{color:'black'}}><span style={{textDecoration:'line-through'}}>₹11,000.00</span> ₹5,199.00</p>
                </div>
                </div>
            </div>
            </div></a>
        </div>
         <div className="landrcolumn2">
              <h4>BEST SELLERS</h4>
            <hr></hr>
            <div style={{display:'flex',flexDirection:'row',justifyContent:'space-around',alignItems:'center'}}>
                <img src={landr1} alt="landr1" style={{width:100,height:100,borderRadius:'50%',marginRight:'10px'}} />
                <p>Free Astrology<br /> Consultation</p>
            </div>
            <hr />
            <div style={{display:'flex',flexDirection:'row',justifyContent:'space-around',alignItems:'center'}}>
                 <img src={landr2} alt="landr1" style={{width:100,height:100,borderRadius:'50%',marginRight:'10px'}} />
                <p>Free Gemstone<br /> Recommendation</p>
            </div>
                <hr />
            <div style={{display:'flex',flexDirection:'row',justifyContent:'space-around',alignItems:'center'}}>
                 <img src={landr3} alt="landr1" style={{width:100,height:100,borderRadius:'50%',marginRight:'10px'}} />
                <p>Astrology<br /> Consultancy</p>
            </div>
        </div>
    </div>
    <Footer />
  </>
 );
}

export default OurServices;
