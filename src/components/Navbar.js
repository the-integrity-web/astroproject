import React from "react";
import '../scss/App.scss';
import '../scss/font.scss';
import search from '../images/Vector.png'
import contact from '../images/Vector (1).png'
import location from '../images/Vector (2).png'
import logo from '../images/logo1.png';
import {Navbar,Nav,NavDropdown,Form} from 'react-bootstrap';


function NavBar() {
  return (
    // <div className="Navbar">
    //     <div className="logo">
    //       <a href="/"> <img className="logo" src={logo} alt="logo" /></a>
    //     </div>
    //    <div className="menu">
    //         <a className="menu_text" href="/aboutus">AboutUs</a>
    //         <a className="menu_text" href="#services">Services</a>
    //         {/* <div className="products_menu"> */}
    //          <a className="menu_text" href="#product">Products</a>
    //          {/* <div className="submenu">
    //           <p>Buy Gems Online</p>
    //           <p>Buy Rudraksha Online</p>
    //           <p>Vastu</p>
    //           <p>Shivaling Idols</p> */}
    //         {/* </div>
    //         </div> */}
    //         <a className="menu_text" href="#blog">Blogs</a>
    //         <a className="menu_text" href="/contact">ContactUs</a>
    //     </div>
    //     <div className="search">
    //        
    //     </div>
        <Navbar className="Navbar" expand="lg">
  <Navbar.Brand href="/"><img className="logo" src={logo} alt="logo" /></Navbar.Brand>
  <Navbar.Toggle aria-controls="basic-navbar-nav"/>
  <Navbar.Collapse id="basic-navbar-nav" >
    <Nav className="mr-auto">
      <Nav.Link href="/aboutus" className="menu_text">AboutUs</Nav.Link>
       <NavDropdown  title="Services" id="basic-nav-dropdown" className="menu_text">
        <NavDropdown.Item href="/loveandrelationship">Love and Relationship</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.2">Vedic Astrology</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.3">Astrology Reports 2020</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.4">Bollywood Astrology</NavDropdown.Item>
          <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.2">Career Astrology</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.3">Child Astrology</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.4">Corporate Astrology</NavDropdown.Item>
          <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.2">Counselling</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.3">Medical Astrology</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.4">Numerology</NavDropdown.Item>
         <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.4">Rudraksha Therapy</NavDropdown.Item>
      </NavDropdown>
      <NavDropdown title="Products" id="basic-nav-dropdown" className="menu_text">
        <NavDropdown.Item href="/mainproduct">Buy Gems Online</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.2">Buy Rudraksha Online</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.3">Vastu</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.4">Shivaling Idols</NavDropdown.Item>
      </NavDropdown>
        <Nav.Link href="/blog" className="menu_text">Blogs</Nav.Link>
          <Nav.Link href="/contact" className="menu_text">ContactUs</Nav.Link>
    </Nav>
    <Form inline>
      <a className="search_text" href=""><img src={search} alt="search" /></a>
     <a className="search_text" href=""><img src={contact} alt="contact" /></a>
    <a className="search_text" href=""><img src={location} alt="location" /></a>
    </Form>
  </Navbar.Collapse>
</Navbar>
    // </div>
  );
}

export default NavBar;
