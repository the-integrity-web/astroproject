import React from "react";
import {Breadcrumb,Form,Col,Button} from 'react-bootstrap'
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import '../scss/services.scss';
import landr1 from '../images/services/landr1.png';
import landr2 from '../images/services/landr2.jpg';
import landr3 from '../images/services/landr3.jpg';
import landr4 from '../images/services/landr4.jpg';
import landr5 from '../images/services/landr5.jpg';
import landr6 from '../images/services/landr6.jpg';
import landr7 from '../images/services/landr7.jpg';
function Product() {
  return (
  <>
    <Navbar />
     <div className="header">
             <Breadcrumb className="bread">
            <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
              <Breadcrumb.Item href="/">Love and Relationship </Breadcrumb.Item>
            <Breadcrumb.Item active>Know your spouse from your Horoscope </Breadcrumb.Item>
            </Breadcrumb>
        </div>
    <div className="productrow">
        <div className="landrcolumn3">
        <div className="product1" style={{padding:'10%'}}>
            <h4>CATEGORIES</h4>
            <hr></hr>
            <p>Astrology Reports 2020</p>
            <p>Bollywood Astrology</p>
            <p>Love and Relationship</p>
            <p>Child Astrology</p>
            <p>Corporate Astrology</p>
            <p>Counselling</p>
             </div>
              <div className="product2" style={{padding:'10%',marginTop:'10%'}}>
              <h4>BEST SELLERS</h4>
            <hr></hr>
            <div style={{display:'flex',flexDirection:'row',justifyContent:'space-around',alignItems:'center'}}>
                <img src={landr1} alt="landr1" style={{width:100,height:100,borderRadius:'50%',marginRight:'10px'}} />
                <p>Free Astrology<br /> Consultation</p>
                          </div>
            <hr />
            <div style={{display:'flex',flexDirection:'row',justifyContent:'space-around',alignItems:'center'}}>
                 <img src={landr2} alt="landr1" style={{width:100,height:100,borderRadius:'50%',marginRight:'10px'}} />
                <p>Free Gemstone<br /> Recommendation</p>
            </div>
                <hr />
            <div style={{display:'flex',flexDirection:'row',justifyContent:'space-around',alignItems:'center'}}>
                 <img src={landr3} alt="landr1" style={{width:100,height:100,borderRadius:'50%',marginRight:'10px'}} />
                <p>Astrology<br /> Consultancy</p>
            </div>
           
        </div>
        </div>
         <div className="landrcolumn4">
             <div className="product_wrapper">
            <div className="product_img">
                <img src={landr5} alt="landr5" style={{marginRight:'20px',borderRadius:'50%'}}/>
            </div>
             <div className="product_title">
                <h4 className="Product_heading">KNOW YOUR SPOUSE FROM YOUR HOROSCOPE</h4>
                <p className="Product_money" style={{textDecoration:'line-through'}}>₹5,400.00 <span className="Product_submoney"  style={{textDecoration:'none'}} > ₹3,600.00</span></p>
            <p className="product_content"><h6 className="Product_heading" >Category: </h6>Love and Relationship</p>
                <p className="product_content"><h6  className="Product_heading">Tags: </h6> future husband analysis through astrology, spouse astrology, spouse compatibility, Spouse horoscope report, spouse report in astrology</p>
            </div>
            </div>
            <h4 className="Product_heading">Know your spouse based on your horoscope</h4>
            <p className="product_content">In a successful married life the relationship with spouse is extremely important. To have a great relationship it is important to know your spouse well. Spouse report in astrology consists of various permutations and combinations of planets. 7th house is considered utmost important for a happy marital life. The combination of malefic planet in 7th house can create problems in your marital life. All theses can be addressed along with  common questions one have. Theses questions are, how my spouse will be? What kind of chemistry we will share? Will I have a harmonious married life with my spouse? Ask for a report which will be calculated on the basis of exact degree of calculations of stars and answers  to all your questions will be provided by our expert.</p>
            <p className="product_content"><span className="Product_submoney">1.</span> In your Spouse Astrology-report, you will get to know:</p>
            <p className="product_content"><span className="Product_submoney">2.</span> Description & details of planets affecting your horoscope; Sun, Moon, Mars, Mercury, Jupiter, Venus, Saturn.</p>
            <p className="product_content"><span className="Product_submoney">3.</span> Combinations & Yogas of all malefic & benefic planets like Sun-Moon-Jupiter-Saturn-Mars & their impact on your married life.</p>
            <p className="product_content"><span className="Product_submoney">4.</span> Planetary description of your spouse with, relevant houses of planets in the chart & their impact on overall personality of your spouse.</p>
            <p className="product_content"><span className="Product_submoney">5.</span> Astrological remedies/solution, lucky gemstone recommendation for pleasant compatibility & harmonious married life.</p>
            <p className="product_content">Although this is a time-bound service which would be covered for an individual chart, however, six month free followup will be available along with this service.</p>
            <h5 className="Product_heading">Input required for providing this service.</h5>
            <p className="product_content">Please provide your complete birth details (Date of birth, Time & Birth Place), and preferred consultation time.</p>
            <p className="product_content">Please note in case of unavailability of birth details one can mention the current time while filling the form including present location. In such special cases, all the astrological calculations and solutions will be provided with the help of Prashna Kundli/Horary Astrology.</p>
            <h6 className="Product_heading">I want to know my future husband through astrology</h6>
            <p className="product_content">In horoscope, the seventh house is the place from where one can know about their spouse. Few planetary combinations like lord, significator of this house and other planetary positions influence to know about your future husband through astrology. While calculating, significator plays a major role in this context. To know the husband’s profile through astrology in females chart, Jupiter is considered as the significator. Similarly in males chart, to know about the future wife, Venus is considered as the significator.</p>
            <h6 className="Product_heading">How my spouse will be?</h6>
            <p className="product_content">This is a very common question and we are often asked this question by unmarried boys and girls. Yes, this is very natural because everyone wants to know about their future spouse and partner. To know about the future spouse through astrology, there can be two ways. First, is the generalised prediction, by an accurate analysis of spouse’s profile from native’s chart. Secondly, by analyzing the ‘choice of a person’ as his or her spouse. You can also check the compatibility of your partner by our specialised service.</p>
            <p className="product_content">Yes indeed, it is possible to know a lot about the spouse through astrology.</p>
            <p className="product_content">In a horoscope, there are various permutations and combinations that could help one to get an exact picture of their future spouse. The lagna lord or the ascendant and the role of lagna lord are very important deciding factor for life partner. Yes, it is obvious to study seventh house for marriage but apart from that only seventh house is not the deciding factor for the choice of a partner. The ascendant lord and its position in a horoscope: its association with the planet; with a malefic planet or with a benefic planet; the strength of degree of ascendant lord, etc. After analysis of the ascendant lord, the fifth house is analysed in detail because this house denotes family, love and children. A person is highly inclined towards a sign which is occupied by a particular zodiac sign. For marital happiness, we study the 7th house. If this house is afflicted or is in shadow of a malefic planet then it might lead to a stressed or unsuccessful marriage. For propelling the good effects of benefic planets in a horoscope, it is essential to follow certain astrological remedies and positive vibration of gems as recommended.</p>
            <h6 className="Product_heading">Astrology Spouse Prediction</h6>
            <p className="product_content">This is a very specialized area of astrology and this need very careful study. On the basis of human ground, it is obvious that prediction may not be 100 percent accurate, but it is a fact that astrological combinations could give you a clear picture and indication of your future would be partner. This could help you to decide upon your future life partner. Your self-confidence will improve, and you could take a better decision regarding your spouse. Astrological analsyis of the horoscope gives a very clear picture regarding the future spouse or life partner. Spouse astrology report can help to get an in-depth analysis of your future husband/wife. It can also help to predict any upcoming problems in your marital life. Spouse astrology is like a boon for the married and unmarried couple.</p>
            <h6 className="Product_heading">Delivery Time: <span className="product_content" style={{color:'white'}}>Three Week</span></h6>
            <h6 className="Product_heading">The report would be Prepared by Astrologer: <span className="product_content" style={{color:'white'}}>Prashant Kapoor</span></h6>
            <div className="birth_form" style={{marginTop:'5%'}}>
                <h3  className="Product_heading">BIRTH INFORMATION</h3>
                  <Form>
                       <Form.Row>
    <Form.Group as={Col} controlId="formGridEmail">
      <Form.Control type="text" placeholder="Name" />
    </Form.Group>
     <Form.Group controlId="exampleForm.ControlSelect1">
    <Form.Control as="select">
        <option>Gender</option>
      <option>Male</option>
      <option>Female</option>
      <option>Transsexual</option>
    </Form.Control>
  </Form.Group>
    <Form.Group as={Col} controlId="formGridPassword">
          <Form.Control as="select">
        <option>Marital status</option>
      <option>Married</option>
      <option>Bachelor</option>
      <option>Widow/Widower</option>
      <option>Divorced</option>
      <option>Live in</option>
    </Form.Control>
    </Form.Group>
  </Form.Row>
  <Form.Row>
    <Form.Group as={Col} controlId="formGridEmail">
      <Form.Control type="date" placeholder="Date of Birth" />
    </Form.Group>

    <Form.Group as={Col} controlId="formGridPassword">
    <Form.Control type="time" placeholder="Time of Birth" />
    </Form.Group>
  </Form.Row>
  <Form.Row>
    <Form.Group as={Col} controlId="formGridEmail">
      <Form.Control type="text" placeholder="Country Of Birth" />
    </Form.Group>

    <Form.Group as={Col} controlId="formGridPassword">
    <Form.Control type="text" placeholder="State Of Birth" />
    </Form.Group>
    <Form.Group as={Col} controlId="formGridPassword">
    <Form.Control type="text" placeholder="Place Of Birth" />
    </Form.Group>
  </Form.Row>
   
     <Form.Row>
    <Form.Group as={Col} controlId="formGridEmail">
      <Form.Control type="text" placeholder="Longitude" />
    </Form.Group>
     <Form.Group as={Col} controlId="formGridEmail">
     <Form.Control as="select">
      <option>East</option>
      <option>West</option>
    </Form.Control>
    </Form.Group>
     </Form.Row>
      <Form.Row>
    <Form.Group as={Col} controlId="formGridPassword">
    <Form.Control type="text" placeholder="Latitude" />
    </Form.Group>
     <Form.Group as={Col} controlId="formGridEmail">
     <Form.Control as="select">
      <option>North</option>
      <option>South</option>
    </Form.Control>
    </Form.Group>
  </Form.Row>
 
  <Button variant="primary" type="submit">
    Submit
  </Button>
</Form>
    
            </div>
            <div className="contact_detail" style={{marginTop:'5%',marginBottom:'5%'}}>
            <h3  className="Product_heading">CONTACT DETAILS</h3>
            <Form>
                <Form.Row>
    <Form.Group as={Col} controlId="formGridEmail">
    <Form.Label style={{color:'purple'}}>Email address</Form.Label>
    <Form.Control type="email" placeholder="Email" />
  </Form.Group>
  <Form.Group as={Col} controlId="formGridEmail">
    <Form.Label style={{color:'purple'}}>Phone/Mobile No</Form.Label>
    <Form.Control type="number" placeholder="Phone/Mobile No" />
  </Form.Group>
  </Form.Row>
   <Form.Row>
  <Form.Group as={Col} controlId="formGridEmail">
    <Form.Label style={{color:'purple'}}>Address</Form.Label>
    <Form.Control as="textarea" rows={3} />
  </Form.Group>
   <Form.Group as={Col} controlId="formGridEmail">
    <Form.Label style={{color:'purple'}}>Your Question</Form.Label>
    <Form.Control as="textarea" rows={3} />
  </Form.Group>
   </Form.Row>
    <Button variant="primary" type="submit">
    Submit
  </Button>
</Form>
</div>
       </div>

       
    </div>
    <Footer />
  </>
 );
}

export default Product;
